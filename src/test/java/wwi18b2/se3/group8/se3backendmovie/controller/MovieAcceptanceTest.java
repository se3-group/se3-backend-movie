package wwi18b2.se3.group8.se3backendmovie.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import wwi18b2.se3.group8.se3backendmovie.model.Movie;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(
        locations = "classpath:application-test.properties")
public class MovieAcceptanceTest {

    @Autowired
    MockMvc mockMvc;

    @Test
    public void postMovie() throws Exception {
        String movieString = generateTestMovieString();

        MvcResult result =mockMvc.perform(MockMvcRequestBuilders.post("/movie")
                .contentType(MediaType.APPLICATION_JSON)
                .content(movieString)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("Batman"))
                .andExpect(jsonPath("$.id").isNotEmpty())
                .andReturn();

        String json = result.getResponse().getContentAsString();
        Movie movie = new ObjectMapper().readValue(json, Movie.class);

        mockMvc.perform(MockMvcRequestBuilders.get("/movie/"+ movie.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("Batman"))
                .andExpect(jsonPath("$.id").isNotEmpty());
    }

    private String generateTestMovieString(){
        return   "{" +
                " \"name\":\"Batman\"," +
                " \"description\":\"I'm Batman!\"," +
                " \"genre\":\"action\"," +
                " \"imageId\":\"BatmanImage.png\"}";
    }
}
