package wwi18b2.se3.group8.se3backendmovie.controller;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import wwi18b2.se3.group8.se3backendmovie.model.Movie;
import wwi18b2.se3.group8.se3backendmovie.repository.MovieRepository;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@ContextConfiguration(classes = MovieController.class)
@WebMvcTest
public class MovieControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MovieRepository repository;


    @Test
    public void getAllMovies() throws Exception {
        List<Movie> movieList = generateTestMovies();

        Mockito.when(repository.findAll()).thenReturn(movieList);

        mockMvc.perform(MockMvcRequestBuilders.get("/movie")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name").value("Batman"))
                .andExpect(jsonPath("$[1].name").value("Spider Man"));
    }

    private List<Movie> generateTestMovies(){
        Movie movie1 = new Movie();
        movie1.setId(1L);
        movie1.setName("Batman");
        movie1.setDescription("I'm Batman!");
        movie1.setGenre("action");
        movie1.setImageId("BatmanImage.png");

        Movie movie2 = new Movie();
        movie2.setId(2L);
        movie2.setName("Spider Man");
        movie1.setDescription("Spiders in action");
        movie1.setGenre("action");
        movie1.setImageId("SpiderManImage.png");

        List<Movie> movieList = new ArrayList<>();
        movieList.add(movie1);
        movieList.add(movie2);
        return movieList;
    }
}
