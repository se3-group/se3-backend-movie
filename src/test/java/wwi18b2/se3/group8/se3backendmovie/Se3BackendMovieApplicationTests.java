package wwi18b2.se3.group8.se3backendmovie;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

@TestPropertySource(
		locations = "classpath:application-test.properties")
@SpringBootTest
class Se3BackendMovieApplicationTests {

	@Test
	void contextLoads() {
	}

}
