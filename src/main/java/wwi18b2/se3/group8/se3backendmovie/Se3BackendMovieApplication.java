package wwi18b2.se3.group8.se3backendmovie;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Se3BackendMovieApplication {

	public static void main(String[] args) {
		SpringApplication.run(Se3BackendMovieApplication.class, args);
	}

}
