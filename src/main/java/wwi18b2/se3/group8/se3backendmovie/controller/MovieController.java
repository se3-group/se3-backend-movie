package wwi18b2.se3.group8.se3backendmovie.controller;

import org.springframework.web.bind.annotation.*;
import wwi18b2.se3.group8.se3backendmovie.model.Movie;
import wwi18b2.se3.group8.se3backendmovie.repository.MovieRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
@CrossOrigin
@RestController
public class MovieController {

    private final MovieRepository movieRepository;

    public MovieController(MovieRepository movieRepository){
        this.movieRepository = movieRepository;
    }

    @PostMapping("/movie")
    public Movie saveMovie(@RequestBody Movie movie){
        return movieRepository.save(movie);
    }

    @GetMapping("/movie/{id}")
    public Optional<Movie> getMovie(@PathVariable Long id){
        return movieRepository.findById(id);
    }

    @GetMapping("/movie")
    public List<Movie> getAllMovies(){
        Iterable<Movie> iterable = movieRepository.findAll();
        List<Movie> movies = new ArrayList<>();
        iterable.forEach(movies::add);
        return movies;
    }

    @PutMapping("/movie/{id}")
    public Movie updateMovie(@PathVariable Long id, @RequestBody Movie movie){
        movie.setId(id);
        return movieRepository.save(movie);
    }

    @DeleteMapping("/movie/{id}")
    public void deleteMovie(@PathVariable Long id){
        movieRepository.deleteById(id);
    }
}
