package wwi18b2.se3.group8.se3backendmovie.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import wwi18b2.se3.group8.se3backendmovie.model.Movie;

public interface MovieRepository extends PagingAndSortingRepository<Movie, Long> {
}
