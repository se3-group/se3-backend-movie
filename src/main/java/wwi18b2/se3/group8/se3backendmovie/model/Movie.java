package wwi18b2.se3.group8.se3backendmovie.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Movie {
    @Id
    @TableGenerator(name = "Movie_Gen", initialValue = 10000)
    @GeneratedValue(strategy= GenerationType.AUTO, generator = "Movie_Gen")
    private Long id;

    private String name;
    @Column(length = 4095)
    private String description;
    private String genre;
    private String imageId;
}
