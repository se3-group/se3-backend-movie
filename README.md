# se3-backend-movie
Dieser Service ist dazu zuständig, Filmdaten zu speichern und bereitzustellen.

### initialer Start der Anwendung:
1. Sicherstellen, dass docker, maven und java 11 installiert sind
2. Datenbank starten mit dem Befehl "docker-compose up" aus dem classpath des Projektes
3. data.sql aus dem resource-Ordner entfernen, damit die Datenbank initial befüllt werden kann (sonst wird versucht die data.sql davor auszuführen)
4. Die main-Methode aus Se3BackendMovieApplication starten
   
Um die Testdaten zu laden:
5. Anwendung beenden
6. data.sql wieder in resource-Ordner legen
7. Anwendung erneut starten 

